<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=egde">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Техническая поддержка</title>
</head>
<body>

<?php require "blocks/header.php"?>
<!--Прошлая версия формы(с использованием check.php)-->
<!--<div class="container mt-5">-->
<!--    <h3>Техническая поддержка</h3>-->
<!--    <form action="check.php" method="post">-->
<!--        <input type="email" name="email" placeholder="Введите Email" class="form-control"><br>-->
<!--        <textarea name="message" class="form-control" placeholder="Введите ваше сообщение"></textarea><br>-->
<!--        <button type="submit" class="w-100 btn btn-lg btn-outline-primary">Отправить</button>-->
<!--    </form>-->
<!--</div>-->

<main class="login-form supForm">
    <div class="cotainer">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Техническая поддержка</div>
                    <div class="card-body">
                        <form action="" method="">
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">E-Mail</label>
                                <div class="col-md-6">
                                    <input type="text"  class="form-control" name="email-address" required autofocus>
                                </div>
                            </div>


                            <div class="container form-group row">
                                <textarea name="message" class="form-control" placeholder="Введите ваше сообщение"></textarea><br>
                            </div>
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Отправить
                                </button>

                            </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

</main>

<?php require "blocks/footer.php"?>

</body>
</html>
