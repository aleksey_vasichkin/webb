<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=egde">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Chicken-call</title>
</head>
<body>



<?php require "blocks/header.php"?>

<main>
    <h3 class="mb-5 text-center">Смартфоны</h3>
    <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Xiaomi Redmi Note 9 <br> 4/128Gb Midnight Grey</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/Phone1.jpg" class="container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">17 990 Руб</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Купить</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Apple iPhone XR 128Gb <br> Black</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/Phone2.jpg" class="container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">52 990 Руб</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Купить</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Apple iPhone 11 Pro 64Gb <br> Тёмно-зелёный</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/Phone3.jpg" class="container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">79 890 Руб</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Купить</button>
                </div>
            </div>
        </div>


    </div>
</main>


<?php require "blocks/footer.php"?>

</body>
</html>
