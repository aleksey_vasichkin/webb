<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=egde">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Chicken-call</title>
</head>
<body>

<?php require_once 'include/db.php'?>

<?php require "blocks/header.php"?>

<main>
    <h3 class="mb-5 text-center">Актуальные предложения</h3>
    <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Cc Free</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">Руб 0<small class="text-muted fw-light">/ Мес</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                                            <li>Срок действия: 3 мес</li>
                                            <li>Интернет: 4 Гб</li>
                                            <li>Минуты и SMS: 100/50</li>
                                            <li>Техническая поддержка</li>
                    </ul>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Тестовый тариф</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Cc Light</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">Руб 240<small class="text-muted fw-light">/ Меc</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Интернет: 7 Гб</li>
                        <li>Минуты и SMS: 200/100</li>
                        <li>Техническая поддержка</li>
                    </ul>
                    <button type="button" class="w-100 btn btn-lg btn-primary">Хочу этот тариф</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Cc Infinity</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">Руб 500<small class="text-muted fw-light">/ Мес</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Интернет: Безлимит</li>
                        <li>Минуты и SMS: Безлимитные звоноки/300</li>
                        <li>Техническая поддержка</li>
                    </ul>
                    <button type="button" class="w-100 btn btn-lg btn-primary">Хочу этот тариф</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm border-primary">
                <div class="card-header py-3 text-white bg-primary border-primary">
                    <h4 class="my-0 fw-normal">Cc PRO</h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title">Руб 399<small class="text-muted fw-light">/ Мес</small></h1>
                    <ul class="list-unstyled mt-3 mb-4">
                        <li>Интернет: 10 Гб</li>
                         <li>Минуты и SMS: 260/150</li>
                         <li>Техническая поддержка</li>
                    </ul>
                    <button type="button" class="w-100 btn btn-lg btn-primary">Хочу этот тариф</button>
                </div>
            </div>
        </div>

    </div>
</main>


<?php require "blocks/footer.php"?>

</body>
</html>
