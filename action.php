<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=egde">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Chicken-call</title>
</head>
<body>

<?php require "blocks/header.php"?>

<main>
    <h3 class="mb-5 text-center">Акции</h3>
    <div class="row row-cols-1 row-cols-md-3 mb-3 text-center">

        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Умные часы и месяц связи в подарок</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/SmartCLock.png" class="img container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">При покупке умных часов получите возможность подключения на тариф «Infinity» с месяцем абонентской платы в подарок!</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Приобрести</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Chicken-call <br> Spotify Premium</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/Spotify.png" class="img container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">Chicken-call дарит <br> всем 6 месяцев <br> подписки <br> на <br> Spotify Premium</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Приобрести</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Chicken-call Подарки — каждый день!</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/Gift.png" class="img container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">Персональные призы от Chicken-call и партнеров разыгрываются ежедневно и бесплатно в сервисе Подарки. Испытайте удачу!</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Приобрести</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Кэшбэк 10% за пополнение баланса услугой «Автоплатеж»</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/autoPay.png" class="img container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">Подключите бесплатную услугу «Автоплатеж» и получайте кэшбэк в размере 10% от суммы каждого автоплатежа целых 6 месяцев!</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Приобрести</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Новые категории повышенного кэшбэка</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/cashBack.png" class="img container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">Получайте 5% в магазинах одежды и детских товаров. <br> В ресторанах быстрого питания. А также за доставку еды из ресторанов и кафе </h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Приобрести</button>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card mb-4 rounded-3 shadow-sm">
                <div class="card-header py-3">
                    <h4 class="my-0 fw-normal">Комплект Chicken-call <br> ТАКСИ</h4>
                </div>
                <div class="card-body">
                    <div>
                        <img src="/img/taxi.png" class="img container mr-btm-50px">
                    </div>
                    <h4 class="text-muted">Специальное предложение для водителей такси, чтобы пользоваться связью на двух устройствах удобнои и легко контролировать расходы.</h4>
                    <button type="button" class="w-100 btn btn-lg btn-outline-primary">Приобрести</button>
                </div>
            </div>
        </div>


    </div>
</main>


<?php require "blocks/footer.php"?>

</body>
</html>
